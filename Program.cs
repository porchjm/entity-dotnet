﻿using System;
using System.Collections.Generic;

namespace entity_dotnet
{
    class Program
  {
    static void Main(string[] args)
    {
      var EM = EntityManager.Instance();
      var e = EM.newEntity();

      EM[e].addComponent(new Component("test", new Dictionary<string, string>() {["payload"] = "4567"}));
      EM[e].addComponent(new Component("otherTest", new Dictionary<string, string>() {["x"] = 20.ToString()}));
      Console.WriteLine(EM.ToString());

      EM[e].update(new[] {
        new Component("test", new Dictionary<string, string>() {["foo"] = "FOO", ["bar"] = "BAR"}),
        new Component("otherTest", new Dictionary<string, string>() {["blah"] = "BLAH", ["baz"] = "BAZ"})
      });
      Console.WriteLine(EM.ToString());
    }
  }
}
